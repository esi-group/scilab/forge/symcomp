/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
#ifndef __COMBINATORICS_HPP__
#define __COMBINATORICS_HPP__

#include "ginac/ginac.h"
using namespace GiNaC;


// Rising factorial
class factorial_rising : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(factorial_rising, basic);
    
public:
    // Constructor of chebyshev of first kind
    factorial_rising(numeric n, ex x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:

    numeric n;
    ex x;
};


// Falling factorial
class factorial_falling : public basic
{
    GINAC_DECLARE_REGISTERED_CLASS(factorial_falling, basic);
    
public:
    // Constructor of chebyshev of first kind
    factorial_falling(numeric n, ex x);

    ex derivative(const symbol & s) const;
    ex expand(unsigned options = 0) const;
    
protected:
    void do_print(const print_context & c, unsigned level = 0) const;
    
private:

    numeric n;
    ex x;
};

#endif /*  __COMBINATORICS_HPP__ */
