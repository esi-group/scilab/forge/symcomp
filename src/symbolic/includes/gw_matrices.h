/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_MATRICES__
#define __GW_MATRICES__
/*--------------------------------------------------------------------------*/ 
#include "machine.h"

#include "gw_base.h"

DEF_FUNCTION(matrix_new);
DEF_FUNCTION(matrix_diag);
DEF_FUNCTION(matrix_unit);
DEF_FUNCTION(matrix_symbolic);
DEF_FUNCTION(matrix_sub);
DEF_FUNCTION(matrix_reduced);
DEF_FUNCTION(matrix_item_get);
DEF_FUNCTION(matrix_item_set);
DEF_FUNCTION(matrix_eval);
DEF_FUNCTION(matrix_determinant);
DEF_FUNCTION(matrix_trace);
DEF_FUNCTION(matrix_charpoly);
DEF_FUNCTION(matrix_rank);
DEF_FUNCTION(matrix_transpose);
DEF_FUNCTION(matrix_size);
DEF_FUNCTION(matrix_size_cols);
DEF_FUNCTION(matrix_size_rows);
DEF_FUNCTION(matrix_to_list);
DEF_FUNCTION(matrix_solve);
DEF_FUNCTION(matrix_inverse);

#endif /*  __GW_MATRICES__ */
