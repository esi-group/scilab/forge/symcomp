/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_LISTS__
#define __GW_LISTS__


#include "gw_base.h"

DEF_FUNCTION(list);
DEF_FUNCTION(list_append);
DEF_FUNCTION(list_prepend);
DEF_FUNCTION(list_item_get);
DEF_FUNCTION(list_item_set);
DEF_FUNCTION(list_remove_first);
DEF_FUNCTION(list_remove_last);
DEF_FUNCTION(list_clear);
DEF_FUNCTION(list_size);
DEF_FUNCTION(list_concatenation);

#endif /*  __GW_LISTS__ */
