// ====================================================================
// Allan CORNET
// DIGITEO 2008
// This file is released into the public domain
// ====================================================================
table = [   'disp'
            'symbol' 
            'number'
            'Pi' 
            'Catalan'
            'Euler' 
            'add'
            'sub'
            'mul'
            'res'
            'pow'
            
            'sin'
            'cos'
            'tan'
            'csc'
            'sec'
            'cot'

            'sinh'
            'cosh'
            'tanh'
            'csch'
            'sech'
            'coth'

            'asin'
            'acos'
            'atan'
            'acsc'
            'asec'
            'acot'

            'asinh'
            'acosh'
            'atanh'
            'acsch'
            'asech'
            'acoth'

            'abs'
            'step'
            'sqrt'
            'exp'
            'log'
            
            'gamma'
            'log_gamma'
            'beta'
            'psi'
            
            'list'
            'list_append'
            'list_prepend'
            'list_item_get'
            'list_item_get'
            'list_item_set'
            'list_remove_first'
            'list_remove_last'
            'list_clear'
            'list_size'
            'list_concatenation'
            
            'matrix_new'
            'matrix_diag'
            'matrix_unit'
            'matrix_symbolic'
            'matrix_sub'
            'matrix_reduced'
            'matrix_item_get'
            'matrix_item_set'
            'matrix_eval'
            'matrix_determinant'
            'matrix_trace'
            'matrix_charpoly'
            'matrix_rank'
            'matrix_transpose'
            'matrix_size'
            'matrix_size_cols'
            'matrix_size_rows'
            'matrix_to_list'
            'matrix_solve'
            'matrix_inverse'

            'poly_degree'
            'poly_ldegree'
            'poly_coeff'
            'poly_lcoeff'
            'poly_tcoeff'
            'poly_quo'
            'poly_rem'
            'poly_gcd'
            'poly_lcm'
            'poly_resultant'
            'poly_sqrfree'
            'poly_factor'

            'poly_chebyshev_T'
            'poly_chebyshev_U'
            'poly_legendre'
            'poly_jacobi'
            'poly_laguerre'
            'poly_gegenbauer'

            'set_digits'
            'get_digits'
            'evalf'
            'expand'
            'collect'
            'subs'

            'derivative'
            'integral_def'
            'integral_eval'

            'scope_clean'
            'scope_list'
            'scope_get'

            'factorial'
            'factorial_rising'
            'factorial_falling'
            
            'index'
            'indexed'
            'index_value'
            'index_dim'
            'index_is_numeric'
            'index_is_symbolic'
            'index_is_dim_numeric'
            'index_is_dim_symbolic'
            'index_is_contravariant'
            'index_is_covariant'
            'index_is_dotted'
            'index_is_undotted'
            'index_toggle_variance'
            'index_toggle_dot'

            'sy_none'
            'sy_symm'
            'sy_anti'
            'sy_cycl'
             
            'get_free_indices'
            'expand_dummy_sum'

            'rel_equal'
            'rel_not_equal'
            'rel_less'
            'rel_less_or_equal'
            'rel_greater'
            'rel_greater_or_equal'
            
            'is_numeric'
            'is_real'
            'is_rational'
            'is_integer'
            'is_crational'
            'is_cinteger'
            'is_positive'
            'is_negative'
            'is_nonnegative'
            'is_posint'
            'is_negint'
            'is_nonnegint'
            'is_even'
            'is_odd'
            'is_prime'
            'is_rel'
            'is_rel_equal'
            'is_rel_not_equal'
            'is_rel_less'
            'is_rel_less_or_equal'
            'is_rel_greater'
            'is_rel_gerater_or_equal'

            'new_function'
            'call_function'
            
            'series'
        ];        // table of (scilab_name,interface-name)

t = [];
for i = 1:size(table,1)
    t = [t; 'sym_' + table(i), 'sci_sym_' + table(i)];
end;

tbx_build_gateway('symbolic', t, ['gw_symbolic.cpp', ...
		        'gw_calculus.cpp', ...
		        'gw_functions.cpp', ...
		        'gw_lists.cpp', ...
		        'gw_polynomials.cpp', ...
		        'gw_matrices.cpp' ...
		        'gw_indexed.cpp' ...
		        'gw_series.cpp' ...
		        'gw_combinatorics.cpp'], ..
                      get_absolute_file_path('builder_gateway_cpp.sce'), ...
		      ['../../src/cpp/libsymbolic_cpp'], ...
		      "-lginac", ...
		      "-I"+get_absolute_file_path('builder_gateway_cpp.sce')+"../../includes");
		      
clear tbx_build_gateway;
clear table;
clear t;
