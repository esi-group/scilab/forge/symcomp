/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_series.h"
}


#include "scope.hpp"
#include "sym.hpp"
#include "ginac/ginac.h"

using namespace GiNaC;
using namespace std;

IMP_FUNCTION(series)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(3,3);

    // Get operands
    ex a            = Sym::get(1);
    relational b    = Sym::get(2);
    unsigned int c  = UInteger::get(3);

    // Do something
    ex res = a.series(b,c);

    // Return data
    Sym(res).put(1);
    return TRUE;
}
