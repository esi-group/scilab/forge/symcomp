/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
 
extern "C" {
#include "stack-c.h"
#include "gw_calculus.h"
}

#include "scope.hpp"
#include "sym.hpp"

#include "ginac/ginac.h"

IMP_FUNCTION(derivative)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex f        = Sym::get(1);
    symbol x    = Sym::get(2);

    // Do something
    ex r = f.diff(x);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(integral_def)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(4,4);

    // Get operands
    ex f        = Sym::get(1);
    symbol x    = Sym::get(2);
    ex a        = Sym::get(3);
    ex b        = Sym::get(4);

    // Do something
    ex r = integral(x,a,b,f);

    // Return data
    Sym(r).put(1);
    return TRUE;
}

IMP_FUNCTION(integral_eval)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(1,1);

    // Get operands
    ex f  = Sym::get(1);

    // Do something
    ex r = f.eval_integ();

    // Return data
    Sym(r).put(1);
    return TRUE;
}

/*IMP_FUNCTION(integral_indef)
{
    // Check input and output size
    CheckLhs(1,1); CheckRhs(2,2);

    // Get operands
    ex f  = Sym::get(1);
    symbol x  = Sym::get(2).to_symbol();

    // Do something
    
    ex r = integral(x,a,b,f);

    // Return data
    Sym(r).put(1);
    return TRUE;
}*/
