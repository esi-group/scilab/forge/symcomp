// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

cur_path = get_absolute_file_path('builder_cpp.sce');

includes_path = cur_path + '../../includes/';

tbx_build_src( ..
	['symbolic_cpp'], ..
	['scope.cpp', ..
	 'sym.cpp', ..
	 'combinatorics.cpp', ..
	 'polynomials.cpp'],..
	'cpp', ..
        get_absolute_file_path('builder_cpp.sce'), ..
	"", ..
	"-lginac", ..
	"-I" + includes_path..
	);

clear tbx_build_src;
