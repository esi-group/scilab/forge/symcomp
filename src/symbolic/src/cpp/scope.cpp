/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009 - Jorge CARDONA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "scope.hpp"
#include <iostream>
#include <sstream>
using namespace std;

// Global scope.
Scope scope;

/// Create a new symbol at the scope and return the representation of it.
string Scope::new_symbol(string name)
{

    map<string,ex>::iterator it = scope.find(name);
    if(it != scope.end())
        return name;

    pair<map<string,ex>::iterator,bool> ret = scope.insert(pair<string, ex>(name,symbol(name)));
    
    return name;
}

string Scope::new_function(string name, unsigned int n)
{    
    map<string,ex>::iterator it = scope.find(name);
    if(it != scope.end())
        return name;
    
    ex f = function(function::register_new(GiNaC::function_options(name, n).dummy()));
    pair<map<string,ex>::iterator,bool> ret = scope.insert(pair<string, ex>(name, f));
    
    if (ret.second == false)
        return name;
    
    return name;
}


string Scope::new_expression(ex e)
{

    ostringstream s;
    s << "#temporal#" << counter;
    
    pair<map<string,ex>::iterator,bool> ret = scope.insert(pair<string, ex>(s.str(),e));

    if (ret.second == false)
        throw logic_error("Repeated temporal");
        
    counter++;
    
    return s.str();
}

