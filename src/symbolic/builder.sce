// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// Simon LIPP
// This file is released into the public domain
// ====================================================================
mode(-1);
lines(0);
try
 v = getversion('scilab');
catch
 error(gettext('Scilab 5.2 or more is required.'));
end;
if v(2) < 2 then
 // new API in scilab 5.2
 error(gettext('Scilab 5.2 or more is required.'));
end
// ====================================================================
if ~with_module('development_tools') then
  error(msprintf(gettext('%s module not installed."),'development_tools'));
end
// ====================================================================
TOOLBOX_NAME = 'symbolic';
TOOLBOX_TITLE = 'Symbolic Toolbox';
// ====================================================================
toolbox_dir = get_absolute_file_path('builder.sce');

tbx_builder_macros(toolbox_dir);
tbx_builder_src(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
// ====================================================================


// mode(-1);
// mainpathB=get_absolute_file_path('builder.sce');
// chdir(mainpathB);
// if isdir('src') then
// chdir('src');
// exec('buildsrc.sce');
// chdir('..');
// end
// if isdir('sci_gateway') then
// chdir('sci_gateway');
// exec('buildsci_gateway.sce');
// chdir('..');
// end
// if isdir('macros') then
// chdir('macros');
// exec('buildmacros.sce');
// chdir('..');
// end
// if isdir('help') then
// chdir('help');
// exec('buildhelp.sce');
// chdir('..');
// end
// clear mainpathB

