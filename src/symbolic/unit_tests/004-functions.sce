// Get current path
pathT = get_absolute_file_path('004-functions.sce');
exec(pathT + '/../etc/symbolic.start');

a = sym_symbol('a');disp(a);

disp(sym_sin(a));
disp(sym_cos(a));
disp(sym_tan(a));
disp(sym_csc(a));
disp(sym_sec(a));
disp(sym_cot(a));

disp(sym_sinh(a));
disp(sym_cosh(a));
disp(sym_tanh(a));
disp(sym_csch(a));
disp(sym_sech(a));
disp(sym_coth(a));

disp(sym_asin(a));
disp(sym_acos(a));
disp(sym_atan(a));
disp(sym_acsc(a));
disp(sym_asec(a));
disp(sym_acot(a));

disp(sym_asinh(a));
disp(sym_acosh(a));
disp(sym_atanh(a));
disp(sym_acsch(a));
disp(sym_asech(a));
disp(sym_acoth(a));

disp(sym_abs(a));
disp(sym_step(a));
disp(sym_sqrt(a));
disp(sym_exp(a));
disp(sym_log(a));

