// Get current path
pathT = get_absolute_file_path('007-polynomials.sce');
exec(pathT + '/../etc/symbolic.start');

x = sym_symbol('x');
n = sym_number('10');

p = sym_poly_chebyshev_T(n, x);
disp(p);disp(sym_expand(p));

p = sym_poly_chebyshev_U(n, x);
disp(p);disp(sym_expand(p));

